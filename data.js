const arr = [
    {
        "gambar":"erlenmeyer",
        "harga":60500,
        "harga_diskon":0,
        "nama":"ERLENMEYER FLASK 100 ML IWAKI LABU ERLENMEYER 100ML 4980-100 ORIGINAL",
        "keterangan":"ERLENMEYER FLASK, NARROW NECK, GRADUATED IWAKI ORIGINAL(Labu erlemeyer) Capacity : 100 ml Catalog : 4980-100",
        "link":"https://tokopedia.link/cj7F7PjfECb"
    },
    {
        "gambar":"ph-meter",
        "harga":580000,
        "harga_diskon":0,
        "nama":"SALINITY PH TDS EC ORP SG TEMP TESTER 7 IN 1 WATER METER SALT C-600",
        "keterangan":"Alat ukur air portable dengan 7 (tujuh) parameter:  Salinitas - Kadar garam / PH - Asam Basa / TDS- Kebersihan air / EC - Elektro Konduktifitas / Temp - Suhu / S.G - Specific Gravity / ORP - Oxidation Reduction Potential",
        "link":"https://tokopedia.link/piL4lbQfECb"
    },
    {
        "gambar":"kamus-inggris",
        "harga":42995,
        "harga_diskon":0,
        "nama":"Buku Kamus Bahasa Indonesia - Inggris",
        "keterangan":"Kamus Bahasa Indonesia - Inggris Pengarang : Jhon M Echols dan Hassan Shadily Penerbit : GRAMEDIA PUSTAKA UTAMA",
        "link":"https://tokopedia.link/w1jlaHsgECb"
    },
    {
        "gambar":"macbookair-1",
        "harga":4150000,
        "harga_diskon":0,
        "nama":"MACBOOK AIR 2015 13 MJVE2 Best Seller bukan 2017 MQD32 MMGF2 - 128 gb",
        "keterangan":"macbook air 2015 second murah",
        "link":"https://tokopedia.link/YUqN9t7gECb"
    },
    {
        "gambar":"xbox-s-1",
        "harga":3999000,
        "harga_diskon":3949000,
        "nama":"Xbox Series S - Console Only",
        "keterangan":"Console Microsoft Xbox murah",
        "link":"https://tokopedia.link/2VROzJntECb"
    },
    {
        "gambar":"n-switch-1",
        "harga":2399000,
        "harga_diskon":0,
        "nama":"Nintendo Switch Lite Console Video Game -yellow - Console Only",
        "keterangan":"Console Nintendo Switch murah",
        "link":"https://tokopedia.link/6asuubFtECb"
    },
    {
        "gambar":"page-5",
        "harga":0,
        "harga_diskon":0,
        "nama":"PIKUMIKU BEADS @ TOKOPEDIA",
        "keterangan":"melayani penjualan tupperware model klasik dan tas mote",
        "link":"https://tokopedia.link/vmkM3PShECb"
    },
    {
        "gambar":"kaos-prg-1",
        "harga":73000,
        "harga_diskon":0,
        "nama":"KAOS PROGRAMMER NODE.JS",
        "keterangan":"Kaos katun biyar semangat kerja",
        "link":"https://tokopedia.link/pWVhndqLFCb"
    },
    {
        "gambar":"kaos-prg-2",
        "harga":89000,
        "harga_diskon":0,
        "nama":"Kaos T-shirt UBUNTU 22.04 Jammy Jellyfish Linux Katun Premium(S-XXL) - S",
        "keterangan":"Kaos katun biyar semangat kerja",
        "link":"https://tokopedia.link/7NgQjJX4ECb"
    },
    {
        "gambar":"toko-eset",
        "harga":0,
        "harga_diskon":0,
        "nama":"ESET OFFICIAL STORE",
        "keterangan":"Melayani produk keamanan PC buatan ESET NOD32",
        "link":"https://tokopedia.link/Rjl6znnkGCb"
    }
    
]

export default arr