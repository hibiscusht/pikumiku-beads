import dflt from "./default.js" 
import arr from "./data.js"


class Home {


    loadMore(json){

        let more = ''

        let items = []

        for(let j = 0; j < 4; j++){

            const rand = Math.floor(Math.random() * json.length)  
             
            items.push(rand) 

        }
 

        for(let n = 0; n < items.length; n++){

            const i = items[n]
  

            const harga = (json[i].harga_diskon != 0)? `<span class="text-muted text-decoration-line-through">Rp ${(new Intl.NumberFormat('id-ID').format(json[i].harga))}</span> Rp ${(new Intl.NumberFormat('id-ID').format(json[i].harga_diskon))}` : `Rp ${(new Intl.NumberFormat('id-ID').format(json[i].harga))}`

            more +=  `

            <div class="col mb-5">
                <div class="card h-100">
                    <!-- Product image-->
                    <img class="card-img-top" src="assets/img/${json[i].gambar}.png" alt="..." style="height: 30%"/>
                    <!-- Product details-->
                    <div class="card-body p-4">
                        <div class="text-center">
                            <!-- Product name-->
                            <h5 class="fw-bolder">${json[i].nama}</h5>
                            <!-- Product price-->
                            ${harga}
                        </div>
                    </div>
                    <!-- Product actions-->
                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                        <div class="text-center"><p class="btn btn-outline-dark mt-auto" onclick="route('${json[i].gambar}')">View options</p></div>
                    </div>
                </div>
            </div> `
  

        }

            return more
    }

    tes(){

        console.log(dflt)

        const harga = (dflt.harga_diskon != 0)? `<span class="text-muted text-decoration-line-through">Rp ${(new Intl.NumberFormat('id-ID').format(dflt.harga))}</span> Rp ${(new Intl.NumberFormat('id-ID').format(dflt.harga_diskon))}` : `Rp ${(new Intl.NumberFormat('id-ID').format(dflt.harga))}`

        const loaded = this.loadMore(arr)

        return `<!-- Product section-->
        <section class="py-5">
            <div class="container px-4 px-lg-5 my-5">
                <div class="row gx-4 gx-lg-5 align-items-center">
                    <div class="col-md-6"><img class="card-img-top mb-5 mb-md-0" src="assets/img/default.png" alt="..." /></div>
                    <div class="col-md-6">
                        <div class="small mb-1"></div>
                        <h1 class="display-5 fw-bolder">${dflt.nama}</h1>
                        <div class="fs-5 mb-5">
                             ${harga}
                        </div>
                        <p class="lead">${dflt.keterangan}</p>
                        <div class="d-flex">
                            <input class="form-control text-center me-3" id="inputQuantity" type="num" value="1" style="max-width: 3rem" />
                            <button class="btn btn-outline-dark flex-shrink-0" type="button"><a href="${dflt.link}" target="_BLANK">
                                <img src="assets/img/tokped.png" />
                                </a>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Related items section-->
            <section class="py-5 bg-light">
                <div class="container px-4 px-lg-5 mt-5">
                    <h2 class="fw-bolder mb-4">Related products</h2>
                    <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center more-goals">
                    ${loaded}
                    </div>
                </div>
            </section>
        `
    }

}
 
const home = new Home()

export default home